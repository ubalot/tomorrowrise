package com.example.tomorrowrise;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.BaseColumns;
import android.provider.CalendarContract;
import android.util.Patterns;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

public class FetchFirstEventForTomorrow extends Service {
    // Projection array. Creating indices for this array instead of doing
    // dynamic lookups improves performance.
    public static final String[] EVENT_PROJECTION = new String[] {
            CalendarContract.Calendars._ID,                           // 0
            CalendarContract.Calendars.ACCOUNT_NAME,                  // 1
            CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,         // 2
            CalendarContract.Calendars.OWNER_ACCOUNT                  // 3
    };

    // The indices for the projection array above.
    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
    private static final int PROJECTION_DISPLAY_NAME_INDEX = 2;
    private static final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;

    public FetchFirstEventForTomorrow() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        final String notificationBeforeBedtime = sharedPreferences.getString("notificationBeforeBedtime", null);
        if (notificationBeforeBedtime == null) {
            Toast.makeText(this, "Please, set \"Notification before bedtime\" in settings", Toast.LENGTH_LONG).show();
        }

        final String bedtimeBeforeSleep = sharedPreferences.getString("bedtimeBeforeSleep", null);
        if (bedtimeBeforeSleep == null) {
            Toast.makeText(this, "Please, set \"Bedtime before sleep\" in settings", Toast.LENGTH_LONG).show();
        }

        final String sleepTime = sharedPreferences.getString("sleepTime", null);
        if (sleepTime == null) {
            Toast.makeText(this, "Please, set \"Sleep time\" in settings", Toast.LENGTH_LONG).show();
        }

        final String noAlarmTime = sharedPreferences.getString("noAlarmTime", null);
        if (noAlarmTime == null) {
            Toast.makeText(this, "Please, set \"Block alarm after\" in settings", Toast.LENGTH_LONG).show();
        }

        final String userEmail = getUserEmail();
        if (userEmail == null) {
            // No user handling.
        }

        Context context = getApplicationContext();
        ContentResolver contentResolver = context.getContentResolver();

        Cursor cursor = getUserCalendar(contentResolver, userEmail);
        while (cursor.moveToNext()) {
            String displayName = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME));
            String accountName = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.ACCOUNT_NAME));
            if (accountName.equals(userEmail)) {
                String[] columnNames = cursor.getColumnNames();
                String[] tmp = columnNames;
                int id = Integer.parseInt(String.valueOf(cursor.getColumnIndex(CalendarContract.Calendars._ID)));
                int n_tmp = id;

                HashMap<String, Integer> tomorrow = tomorrowData();

                String[] eventsProjection = new String[] {
                        CalendarContract.Events._ID,
                        CalendarContract.Events.DTSTART,
                        CalendarContract.Events.DTEND,
                        CalendarContract.Events.TITLE,
                        CalendarContract.Events.AVAILABILITY
                };
                String eventsSelection = "(" + CalendarContract.Events.ALL_DAY + " = 0)";
                Cursor eventsCursor = contentResolver.query(CalendarContract.Events.CONTENT_URI, eventsProjection, eventsSelection, null, CalendarContract.Events.DTSTART + " ASC");
                while (eventsCursor.moveToNext()) {
                    int title = cursor.getColumnIndex(CalendarContract.Events.TITLE);
                    if (title != -1) {
                        String eventName = cursor.getString(title);
                        String __tmp = eventName;
                    }
                }
            }
        }
        cursor.close();

        /*
        START_STICKY
        the system will try to re-create your service after it is killed

        START_NOT_STICKY
        the system will not try to re-create your service after it is killed
         */
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }

    private String getUserEmail() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        AccountManager accountManager = AccountManager.get(this);
        Account[] accounts = accountManager.getAccountsByType("com.google");
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                return account.name;
            }
        }
        return null;
    }

    private Cursor getUserCalendar(ContentResolver contentResolver, String userEmail) {
        final Uri uri = CalendarContract.Calendars.CONTENT_URI;
        final String[] projection = {
                CalendarContract.Calendars.ALLOWED_ATTENDEE_TYPES,
                CalendarContract.Calendars.ACCOUNT_NAME,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                CalendarContract.Calendars.CALENDAR_LOCATION,
                CalendarContract.Calendars.CALENDAR_TIME_ZONE,
                CalendarContract.Calendars._ID,
        };
        String selection = "(" + CalendarContract.Calendars.ACCOUNT_NAME + " = '" + userEmail + "')";
//        String[] selectionArgs = new String[] {
//                userEmail,
//                "cal.zoftino.com",
//                userEmail
//        };
        return contentResolver.query(uri, projection, selection, null, null);
    }

    private HashMap<String, Integer> tomorrowData() {
        Date date = new Date();
//        final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final DateFormat dayFormat = new SimpleDateFormat("dd");
        final DateFormat monthFormat = new SimpleDateFormat("MM");
        final DateFormat hourFormat = new SimpleDateFormat("hh");
//        String now = sdf.format(date);
        int hour = Integer.parseInt(hourFormat.format(date));

        int day = Integer.parseInt(dayFormat.format(date));
        int month = Integer.parseInt(monthFormat.format(date));

        int tomorrowDay = day++;

        HashMap<String, Integer> tomorrow = new HashMap<String, Integer>();
        tomorrow.put("day", tomorrowDay);
        return tomorrow;
    }
}
