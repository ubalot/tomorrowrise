package com.example.tomorrowrise;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
//import android.preference.EditTextPreference;
//import android.preference.ListPreference;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
//import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

//        Switch activeSwitch = findViewById(R.id.active);

//        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

    }

    /* Handle AppBar navigation */
    @Override
    public boolean onSupportNavigateUp() {
        Intent mainActivity = new Intent(this, MainActivity.class);
        if (mainActivity != null) {
            startActivity(mainActivity);
            return true;
        }
        return false;
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        private final String[] preferenceKeys = {
                "notificationBeforeBedtime",
                "bedtimeBeforeSleep",
                "sleepTime",
                "noAlarmTime"
        };

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            for (String prefKey : preferenceKeys) {
                ListPreference preference = (ListPreference) findPreference(prefKey);
                preference.setSummaryProvider(new Preference.SummaryProvider<ListPreference>() {
                    @Override
                    public CharSequence provideSummary(ListPreference preference) {
                        String text = preference.getValue();
                        if (TextUtils.isEmpty(text)) {
                            return "Not set";
                        }
                        return text;
                    }
                });
            }
        }
    }
}